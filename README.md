# Infograph Typeface

Infograph is a typeface based on Inter 3.15 (please see LICENSE.txt for copyright information regarding that typeface). The Hinted fonts are to be used with Windows.

Each folder contains two variants - one with a slashed zero (SlashedZero) and one without (ClearZero). The typeface also uses tabular numbers, an alternate number set, and the modification of the glyphs lowercase L and uppercase G.

# Motivation

I decided to create this fork, with the help of `pyftfeatfreeze`, for certain reasons:

- Inter is an inspirational typeface for me in that users have contributed to adding a huge variety of alternate letter forms (more than what most other sans-serif typefaces provide).
- Inter has a wide coverage of languages using the Latin, Greek and Cyrillic writing systems - this is important in allowing information of less spoken languages to be more efficiently disseminated online that accurately reflects their orthography.
- I didn't like the default proportional digits offered by default in Inter - I found it distracting to read especially when using it as a UI font with a digital clock.
- With Cyrillic being another widely used writing system, I found it unacceptable that Cyrillic letter Ze and Hindu-Arabic digit 3 looked similar. This is problematic especially for most sans-serif typefaces and even a handful of serif typefaces like Droid Serif, Liberation Serif and their respective derivatives. I find it also unacceptable that Cyrillic letter Be and Hindu-Arabic digit 6 looked similar. A typeface like Lato is actually effective in addressing such ambiguities with geometric digits in an otherwise humanist typeface.
- I dislike that many sans-serif typefaces fail to render lowercase L and uppercase i in a distinct manner (including the aforementioned Lato typeface). This can be quite challenging to speakers of languages that use the Latin script when they come across unfamiliar words, names or online handles, and can often be abused in contexts where usernames are primarily or exclusively used to identify users.
- I decided to use a spurred letter G out of a stylistic choice, initially thinking it appeared superfluous. I wanted this typeface to be a free and open source replacement for Helvetica, without imitating the spurred R glyph and ambiguous letters which I personally dislike.

# Limitations

So far, as with Inter, there are no true italics to the typeface, only obliques. I would wish to be able to replace the tittles and punctuation with a more squarish shape, but this is currently beyond my technological capabilities and outside my preferred commitments. This project may lie dormant and remain available only for others to be able to access, download and fork. Please do not expect fixes and updates to occur, especially as most of the work on the typeface is to be credited by the Inter Project Authors. I currently believe this typeface to be sufficient for many use cases at its current stage.